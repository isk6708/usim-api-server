<?php

namespace App\Http\Controllers;

use App\Models\Hr\Staff;
use Illuminate\Http\Request;



class StaffController extends Controller
{
    /**
     * @OA\Get(
     *     path="/senarai-staff",
     *     tags={"Maklumat Kakitangan"},
     *     summary="Senarai Kakitangan",
     *     description="Multiple status values can be provided with comma separated string",
     *     operationId="index",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid status value"
     *     ),
    *     security={
     *         {"api_key_usim": {}}
     *     }
     * )
     */

    public function index()
    {
        // dd('test');
        return Staff::all();
    }

   
    /**
     * @OA\Post(
     *     path="/simpan-staff",
     *     tags={"Simpan Maklumat Kakitangan"},
     *     summary="Simpan Rekod Kakitangan",
     *     description="Multiple status values can be provided with comma separated string",
     *     operationId="store",
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="Status values that needed to be considered for filter",
     *         required=true,
     *         explode=true,
     *         @OA\Schema(
     *             default="available",
     *             type="string",
     *             enum={"available", "pending", "sold"},
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid status value"
     *     ),
     * )
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Staff $staff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Staff $staff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Staff $staff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Staff $staff)
    {
        //
    }
}
