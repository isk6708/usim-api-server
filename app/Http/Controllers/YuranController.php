<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class YuranController extends Controller
{
    /**
     * @OA\Get(
     *     path="/senarai-pembayar-yuran",
     *     tags={"Senarai Pembayar Yuran"},
     *     summary="Senarai Pembayar Yuran",
     *     description="Senarai Pembayar Yuran, Jumlah Yuran Yang Dibayar, Jumlah Yuran Tertunggak",
     *     operationId="senaraiPembayar",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid status value"
     *     ),
     * )
     */
    public function senaraiPembayar(){

    }
}
