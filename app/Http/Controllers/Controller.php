<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="USIM Centralise API Server",
 *      description="USIM Centralise API Serverdescription",
 *      @OA\Contact(
 *          email="zaihan@usim.edu.my"
 *      ),
 * )
 *  @OA\Server(
 *     description="SwaggerHUB API Mocking",
 *     url="http://10.8.154.17/usim-api-server/public"
 * )
 */

 
class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
}
