<?php

namespace Database\Seeders;

use App\Models\Hr\Staff;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('staffs')->delete();
        
        $arr = [
            ['user_id'=>1,'staff_no'=>'S1234','gender'=>'L','marital_status'=>'10', 'dob'=> '1978-12-13','permanent_addr'=>'KL Menjerit'],
            ['user_id'=>2,'staff_no'=>'S4321','gender'=>'P','marital_status'=>'10', 'dob'=> '1979-11-20','permanent_addr'=>'Sri Tanjung'],
            ['user_id'=>3,'staff_no'=>'D7781','gender'=>'L','marital_status'=>'11', 'dob'=> '1980-09-10','permanent_addr'=>'Setiu'],
        ];
        Staff::insert($arr);
    }
}
