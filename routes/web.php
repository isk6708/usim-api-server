<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    // echo 'Hello USIM';
    return view('welcome');
});



Route::get('/belajar-api',function(){
    echo 'Mari belajar api';
})->name('belajar.api');

Route::get('/statistik-daftar',function(){
    echo 'Statistik berdaftar';
});

Route::prefix('ais')->group(function () {
    Route::get('/jadual-waktu/{course}/{semester?}/{tahun?}',function($course,$semester='',$tahun=''){
        echo 'jadual-waktu '.$course.' - '.$semester.' - '.$tahun;
    })->name('jadual.waktu');
    Route::prefix('yuran')->group(function () {
        Route::get('/yuran-pelajar',function(){
            echo 'yuran belajar';
        })->name('yuran.pelajar');
    });
});

Route::prefix('hr')->group(function () {
    @include('hr/cuti.php');
    @include('hr/staff.php');
});


Route::prefix('hutang')->group(function () {
    Route::get('/hutang-belajar/{tahun?}/{course?}',function($tahun='',$course=''){
        echo 'hutang belajar';
    });
    
    Route::post('/bayar-hutang-belajar',function(){
        echo 'bayar hutang belajar';
    })->name('bayar.hutang.belajar');
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::middleware('auth:api')->get('/senarai-staff', [App\Http\Controllers\StaffController::class, 'index'])->name('senarai-staff');
Route::get('/simpan-staff', [App\Http\Controllers\StaffController::class, 'store'])->name('simpan.staff');
Route::get('/demo-login', [App\Http\Controllers\LoginController::class, 'login'])->name('login.staff');
