<?php

use App\Http\Controllers\StaffController;

Route::prefix('personnel')->group(function () {        
        Route::get('/maklumat-staff',[StaffController::class,'index'])->name('maklumat.staff');
});